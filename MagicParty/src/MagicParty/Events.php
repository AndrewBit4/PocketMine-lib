<?php
/**
 * Created by PhpStorm.
 * User: AndrewBit
 * Date: 24/06/2016
 * Time: 23:11
 */

namespace MagicParty;

use pocketmine\event\Listener;

class Events implements Listener {

    /** @var Base */
    private $plugin;

    /**
     * Events constructor.
     * @param Base $plugin
     */
    public function __construct(Base $plugin) {
        $plugin->getServer()->getPluginManager()->registerEvents($this, $plugin);
        $this->plugin = $plugin;
    }

}