<?php
/**
 * Created by PhpStorm.
 * User: AndrewBit
 * Date: 24/06/2016
 * Time: 23:23
 */

namespace MagicParty\party;

use MagicParty\Base;

class PartyManager {

    /** @var Base */
    private $plugin;

    /**
     * PartyManager constructor.
     * @param Base $plugin
     */
    public function __construct(Base $plugin) {
        $this->plugin = $plugin;
    }

}