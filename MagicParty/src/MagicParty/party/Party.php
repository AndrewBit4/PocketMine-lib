<?php
/**
 * Created by PhpStorm.
 * User: AndrewBit
 * Date: 24/06/2016
 * Time: 23:22
 */

namespace MagicParty\party;

use pocketmine\Player;

class Party {

    /** @var Player */
    private $owner;

    // /** @var array */
    // private $members = array();

    /**
     * Party constructor.
     * @param Player $owner
     */
    public function __construct(Player $owner) {
        $this->owner = $owner;
    }

    /**
     * @return Player
     */
    public function getOwner() {
        return $this->owner;
    }

    /**
     * @param Player $owner
     */
    public function setOwner(Player $owner) {
        $this->owner = $owner;
    }

}