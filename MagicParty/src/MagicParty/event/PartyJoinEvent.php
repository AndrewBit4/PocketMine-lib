<?php
/**
 * Created by PhpStorm.
 * User: AndrewBit
 * Date: 24/06/2016
 * Time: 23:28
 */

namespace MagicParty\event;

use MagicParty\Base;
use MagicParty\party\Party;
use pocketmine\event\Cancellable;
use pocketmine\event\plugin\PluginEvent;
use pocketmine\Player;

class PartyJoinEvent extends PluginEvent implements Cancellable {

    public static $handlerList = null;

    /** @var Player */
    private $player;

    /** @var Party */
    private $party;

    /**
     * PartyJoinEvent constructor.
     * @param Base $plugin
     * @param Player $player
     * @param Party $party
     */
    public function __construct(Base $plugin, Player $player, Party $party) {
        parent::__construct($plugin);
        $this->player = $player;
        $this->party = $party;
    }

    /**
     * @return Player
     */
    public function getPlayer() {
        return $this->player;
    }

    /**
     * @return Party
     */
    public function getParty() {
        return $this->party;
    }

}