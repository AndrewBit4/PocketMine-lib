<?php
/**
 * Created by PhpStorm.
 * User: AndrewBit
 * Date: 24/06/2016
 * Time: 23:10
 */

namespace MagicParty;

use MagicParty\party\PartyManager;
use pocketmine\plugin\PluginBase;
use pocketmine\utils\Config;

class Base extends PluginBase {

    /** @var Events */
    private $listener;

    /** @var Config */
    private $config;

    /** @var PartyManager */
    private $partyManager;

    public function onEnable() {
        $this->initialize();
        $this->setConfig();
        $this->setPartyManager();
        $this->setListener();
        $this->getLogger()->info("MagicParty was loaded.");
    }

    public function onDisable() {
        $this->getLogger()->info("MagicParty was disabled.");
    }

    /**
     * @return Events
     */
    public function getListener() {
        return $this->listener;
    }

    /**
     * @return Config
     */
    public function getConfig() {
        return $this->config;
    }

    /**
     * @return PartyManager
     */
    public function getPartyManager() {
        return $this->partyManager;
    }

    public function setListener() {
        $this->listener = new Events($this);
    }

    public function setConfig() {
        $this->config = json_decode(file_get_contents($this->getDataFolder() . "settings.json"), true);
    }

    public function setPartyManager() {
        $this->partyManager = new PartyManager($this);
    }

    private function initialize() {
        if(!is_dir($this->getDataFolder())) {
            mkdir($this->getDataFolder());
        }
        $this->saveResource("settings.json");
    }

}