<?php
/**
 * Created by PhpStorm.
 * User: AndrewBit
 * Date: 23/06/2016
 * Time: 20:16
 */

namespace QuickUHC;

use pocketmine\plugin\PluginBase;
use pocketmine\utils\TextFormat;

class Base extends PluginBase {

    /** @var Events */
    private $listener;

    /** @var Base */
    private static $object = null;

    public function onLoad() {
        if(!self::$object instanceof Base) {
            self::$object = $this;
        }
    }

    public function onEnable() {
        $this->initialize();
        $this->setListener();
        $this->getLogger()->info("QuickUHC by " . TextFormat::AQUA . "@AndrewBit4 " . TextFormat::RESET . "enabled");
    }

    public function onDisable() {
        $this->getLogger()->info("QuickUHC by " . TextFormat::AQUA . "@AndrewBit4 " . TextFormat::RESET . "enabled");
    }

    /**
     * @return Base
     */
    public static function getInstance() {
        return self::$object;
    }

    /**
     * @return Events
     */
    public function getListener() {
        return $this->listener;
    }

    public function setListener() {
        $this->listener = new Events($this);
    }

    public function initialize() {
        if(!is_dir($this->getDataFolder())) {
            mkdir($this->getDataFolder());
        }
        $this->saveResource("data.json");
        $this->saveResource("settings.json");
    }

}